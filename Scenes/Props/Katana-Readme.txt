Katana
By Ken Nign

Model built and surfaced in LW 9.0. Surfacing is done with nodes, all
coloring is done via nodes, allowing easy color changes. All of the
images are just bump maps or used as alpha masks for different textures.

No third-party nodes are needed.

Subpatches were used to allow for variable levels of detail at distance.


Ken Nign
3/12/2008